module Utils where

import Control.Applicative
import Data.Traversable (sequenceA)
import Data.List (tails)

-- Transposition
transpose' :: [[a]] -> [[a]]
transpose' = getZipList . sequenceA . map ZipList

-- Sliding window
windows :: Int -> [a] -> [[a]]
windows m = transpose' . take m . tails

