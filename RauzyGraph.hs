module RauzyGraph where

import Data.Set (fromList,toList)
import qualified Data.Map as Map
import Word
import Utils (windows)

-- Rauzy graphs
data Arc = Arc Word Word Word deriving (Eq,Ord)
type RauzyGraph = [Arc]

instance Show Arc where
    show (Arc u l v) = "  " ++ show u ++ " -> " ++ show v ++
                       " [label=" ++ show l ++ "];\n"

rauzyGraphFromWord :: Word       -- The word of which the Rauzy graph is desired
                   -> Int        -- The order of the Rauzy graph
                   -> RauzyGraph -- The Rauzy graph of given order of the word
rauzyGraphFromWord w n = toList $ fromList $ map makeArc $ windows (n+1) w
    where makeArc w = Arc (init w) w (tail w)

graphvizStringFromRauzyGraph :: RauzyGraph -> String
graphvizStringFromRauzyGraph rg = "digraph {\n" ++ concatMap show rg ++ "}\n"
