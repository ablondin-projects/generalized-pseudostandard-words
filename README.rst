Mots pseudostandards généralisés
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. default-role:: math

L'objectif de ce dépôt est de fournir des fonctions pour étudier la complexité
des mots pseudostandards généralisés, c'est-à-dire des mots obtenus par clôture
pseudopalindromique itérée.

Un des meilleurs outils pour étudier la complexité est appelé *graphe de Rauzy*.
Il s'agit d'un graphe orienté dont les sommets sont les facteurs de longueur
fixée `n` et il existe un arc d'un sommet `u` vers un sommet `v` étiqueté `w` si
`w` est de longueur `n + 1`, `u` est un préfixe de `w` et `v` est un suffixe de
`w`.

La prochaine étape consiste à implémenter la fonction qui calcule le graphe de
*Rauzy réduit*...
