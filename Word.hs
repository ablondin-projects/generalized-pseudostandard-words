module Word where

import Data.List (tails)
import Data.Set (fromList,toList)
import Utils (windows)

-- Words
type Letter   = Char
type Word     = [Letter]
type Alphabet = [Letter]

factorsN :: Word -> Int -> [Word]
factorsN w 0 = [""]
factorsN w n = toList $ fromList $ windows n w

factors :: Word -> [Word]
factors w = concat [factorsN w n | n <- [0..length w]]

complexity :: Word -> [Int]
complexity w = [length (factorsN w i) | i <- [0..]]

-- Involutory antimorphisms

type InvolutoryAntimorphism = Word -> Word

exchange :: Alphabet -> InvolutoryAntimorphism
exchange [a,b] = reverse . map (swap a b)
    where swap a b c | a == c = b
                     | b == c = a
exchange _ = error "Alphabet is not binary"

e :: InvolutoryAntimorphism
e = exchange "01"

r :: InvolutoryAntimorphism
r = reverse

-- Pseudopalindromes

isPalindrome :: Word -> Bool
isPalindrome w = w == reverse w

isPseudopalindrome :: InvolutoryAntimorphism -> Word -> Bool
isPseudopalindrome inv w = w == inv w

pseudopalindromicClosure :: InvolutoryAntimorphism -> Word -> Word
pseudopalindromicClosure inv w = w ++ inv pref
    where pref = take (length w - llps) w
          llps = head $ map length ps
          ps   = filter (isPseudopalindrome inv) (tails w)

palindromicClosure :: Word -> Word
palindromicClosure = pseudopalindromicClosure reverse

-- Iterated closure

type Bisequence = [(InvolutoryAntimorphism, Letter)]

iteratedPseudopalindromicClosure :: Bisequence -> Word
iteratedPseudopalindromicClosure = ipc . reverse
    where ipc :: Bisequence -> Word
          ipc [] = ""
          ipc ((i,l):bs) = pseudopalindromicClosure i $ ipc bs ++ [l]

iteratedPalindromicClosure :: Word -> Word
iteratedPalindromicClosure = iteratedPseudopalindromicClosure . zip (repeat reverse)

lubka :: Int -> Word
lubka n = iteratedPseudopalindromicClosure $ take n $ zip (cycle [e,e,e,r,r,r]) (repeat '1')

-- Special factors

data Side = LeftSide | RightSide

isSpecial :: Side -> Word -> Word -> Bool
isSpecial s w u = (length $ filter (isExtension s u) $ factorsN w (length u + 1)) > 1
    where isExtension LeftSide  x y = (x == init y)
          isExtension RightSide x y = (x == tail y)

specialFactors :: Side -> Word -> Int -> [Word]
specialFactors s w n = filter (isSpecial s w) (factorsN w n)

