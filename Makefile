EXEC=Main
MAIN=Main
MODULES=RauzyGraph.hs
GRAPH=graph

$(GRAPH).pdf: $(EXEC)
	./$(EXEC) | neato -Tpdf -Goverlap=false > graph.pdf

$(EXEC): $(MAIN).hs $(MODULES)
	ghc -o $(EXEC) $(MAIN).hs

